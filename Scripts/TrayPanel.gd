extends Control

signal clicked(type)

export (int) var number = 0
export (int) var max_number
export (String) var type
export (PackedScene) var Piece
export (int) var offset = 12

var pieces = []

func _ready():
	$TextureButton.disabled = true
	for i in range(max_number):
		var piece = Piece.instance()
		piece.set_piece(type)
		add_child(piece)
		piece.margin_left = i * offset
		piece.hide()
		pieces.append(piece)

func flip_labels():
	for p in pieces:
		p.get_node("Label").set_flip_v(true)

func disable():
	$TextureButton.disabled = true

func enable(): # TODO more complications later
	$TextureButton.disabled = number == 0 

func add_piece():
	pieces[number].show()
	number += 1

func remove_piece():
	number -= 1
	pieces[number].hide()

func _on_TextureButton_pressed():
	emit_signal("clicked", type)
