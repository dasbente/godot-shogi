extends Node2D

##### SIGNAL LOGIC #####
export (int) var row
export (int) var col

signal pressed(cell_v)

func _on_TextureButton_pressed():
	emit_signal("pressed", Vector2(col, row))

##### RENDER LOGIC #####
export (Color) var c_promote = Color(0.4, 1.0, 0.4, 1.0)
export (Color) var c_critical = Color(1.0, 0.0, 0.0, 1.0)
export (Color) var c_disabled = Color(0.4, 0.4, 0.4, 1.0)

func _ready():
	$Piece.hide()

func set_piece(piece):
	$Piece.set_piece(piece)
	$Piece.show()

func danger():
	$Sprite.modulate = c_critical

func promote():
	$TextureButton.modulate = c_promote

func reset():
	$TextureButton.modulate = Color.white
	$Sprite.modulate = Color.white

func disable(can_move):
	$TextureButton.disabled = not can_move
	if not can_move:
		$Sprite.modulate = c_disabled

func draw_tile(info):
	reset()
	disable(info["can_move"])
	
	if info.has("piece"):
		set_piece(info["piece"])
	else:
		$Piece.hide()
	if info["critical"]:
		danger()
	if info["can_promote"]:
		promote()
