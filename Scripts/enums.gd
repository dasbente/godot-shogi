extends Node

# UP and DOWN are the two player directions
# CURRENT is intended to be a stand-in for current player
# OPPONENT is intended to be the opposite of current player
# AS_OPPONENT is intended to be current player for calculating enemy movement
enum Facing {
	UP, DOWN, CURRENT, OPPONENT, 
}

func opposite(dir):
	match dir:
		Facing.DOWN:
			return Facing.UP
		Facing.UP:
			return Facing.DOWN
		Facing.CURRENT:
			return Facing.OPPONENT
		Facing.OPPONENT:
			return Facing.CURRENT

const TYPES = ["king", "pawn", "lance", "knight", "gold", "silver", "rook", "bishop"]