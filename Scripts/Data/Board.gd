extends Node

var pieces = {}
var _dimensions

func _init(width = 9, height = 9):
	_dimensions = Vector2(width, height)

func on_board(v):
	return Rect2(Vector2(), _dimensions).has_point(v)

func valid_position(v, ally_dir):
	return free_pos(v) or not pieces[v].dir == ally_dir

func free_pos(v):
	return on_board(v) and not pieces.has(v)

func is_dir(v, dir):
	return pieces.has(v) and pieces[v].dir == dir

func add_piece(piece, at):
	var capture = remove_piece(at)
	pieces[at] = piece
	return capture

func remove_piece(at):
	var piece = null
	if pieces.has(at):
		piece = pieces[at]
		pieces.erase(at)
	return piece

func make_move(from, to):
	var piece = remove_piece(from)
	return add_piece(piece, to)