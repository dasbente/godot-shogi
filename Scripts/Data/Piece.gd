extends Node

var Facing = enums.Facing

var type
var dir
var promoted

var moves = null

func _get_moves():
	if moves != null:
		return moves # no recalculation needed
	
	var off_x = []
	var off_y = []
	var res = {"hop": [], "stride": []}
	var down = dir == Facing.DOWN
	
	if type == "king" or promoted and (["rook", "bishop"].has(type)):
		off_x = [-1, 0, 1, 1, 1, 0, -1, -1]
		off_y = [-1, -1, -1, 0, 1, 1, 1, 0]
	elif type == "gold" or promoted and not ["king", "rook", "bishop"].has(type):
		off_x = [-1, -1, 0, 1, 1, 0]
		off_y = [0, -1, -1, -1, 0, 1]
	elif type == "silver":
		off_x = [-1, 0, 1, -1, 1]
		off_y = [-1, -1, -1, 1, 1]
	elif type == "knight":
		off_x = [-1, 1]
		off_y = [-2, -2]
	elif type == "pawn":
		off_x = [0]
		off_y = [-1]
	elif type == "lance":
		res["stride"] = [Vector2(0,1) if down else Vector2(0,-1)]
	
	var v = Vector2(0, 1)
	var h = Vector2(1, 0)
	if type == "bishop":
		for vect in [v + h, v - h, h - v, -h - v]:
			res["stride"].append(vect)
	elif type == "rook":
		for vect in [v, h, -v, -h]:
			res["stride"].append(vect)
	
	if down:
		for i in range(off_y.size()):
			off_y[i] *= -1
	
	for i in range(off_x.size()):
		res["hop"].append(Vector2(off_x[i], off_y[i]))
	
	return res

func ally(piece):
	return dir == piece.dir

func next_positions(board, v, as_opponent = false):
	var moves = _get_moves()
	var res = []
	var blocked = not as_opponent
	
	for m in moves["hop"]:
		var next = v + m
		if board.on_board(next) and (as_opponent or not board.is_dir(next, dir)):
			res.append(next)
	
	for m in moves["stride"]:
		var next = v
		while board.on_board(next):
			next += m
			if board.free_pos(next) or as_opponent or not board.is_dir(next, dir):
				res.append(next)
			if board.pieces.has(next):
				if board.is_dir(next, dir) == as_opponent or blocked:
					break
				else:
					blocked = true
	
	return res

func flip():
	dir = enums.opposite(dir)

func promote():
	promoted = true

func capture():
	flip()
	moves = null
	if not ["king", "gold"].has(type):
		promoted = false

func can_promote():
	return not promoted

func may_promote(board, v):
	return can_promote() and (
		dir == Facing.UP and v.y <= 2 or dir == Facing.DOWN and v.y >= 6
	)

func must_promote(v):
	var bound
	match type:
		"lance", "pawn":
			bound = 0
		"knight":
			bound = 1
		_:
			return false
	return dir == Facing.DOWN and v.y >= 8.0 - bound or dir == Facing.UP and v.y <= bound

func _init(new_type, new_dir):
	dir = new_dir
	type = new_type
	promoted = ["king", "gold"].has(type)
	
	