extends Node2D

var Facing = enums.Facing
var current_player = Facing.UP

var tiles = {}

var Piece = preload("res://Scripts/Data/Piece.gd")
var board

var trays

##### GAME FLOW LOGIC #####
enum TurnState {
	START, PIECE_SELECTED, RELEASE_SELECTED, MAY_PROMOTE
}

var current_state = TurnState.START
var selected
var board_state

var TYPES = enums.TYPES

func switch_player():
	"""
	Switch the player currently in control.
	"""
	trays[current_player].disable()
	current_player = opposite(current_player)
	trays[current_player].enable()
	
	# Setup for a new turn
	board_state = get_board_state()
	board_state["release_options"] = calc_release_options(board_state)
	for t in TYPES:
		if not board_state["release_options"].has(t):
			trays[current_player].disable_one(t)
	
	current_state = TurnState.START

func opposite(dir):
	"""
	Returns the opposite of a given direction.
	"""
	return to_dir(enums.opposite(dir))

func to_dir(dir):
	"""
	Flattens a direction into either UP or DOWN.
	"""
	match dir:
		Facing.CURRENT:
			return current_player
		Facing.OPPONENT:
			return opposite(current_player)
		_:
			return dir

func allies(dir1, dir2 = Facing.CURRENT):
	"""
	Checks if two directions are the same. If one is default, comparison is with current_player.
	"""
	return to_dir(dir1) == to_dir(dir2)

##### BOARD LOGIC #####
func new_piece_at(type, dir, cell_v):
	"""
	Adds a new piece to the board.
	@param type: Type of the piece
	@param dir: facing direction
	@param cell_v: Position of the piece on the board
	"""
	var p = Piece.new(type, dir)
	board.add_piece(p, cell_v)
	return p

func generate_standard_game():
	"""
	Initializes the board for a standard game of Shogi.
	"""
	# Place pawns
	for col in range(9):
		new_piece_at("pawn", Facing.DOWN, Vector2(col, 2))
		new_piece_at("pawn", Facing.UP, Vector2(col, 6))
	
	# Other pieces
	new_piece_at("rook", Facing.DOWN, Vector2(1,1))
	new_piece_at("bishop", Facing.DOWN, Vector2(7,1))
	new_piece_at("rook", Facing.UP, Vector2(7, 7))
	new_piece_at("bishop", Facing.UP, Vector2(1, 7))
	
	var types = ["lance", "knight", "silver", "gold"]
	for i in range(4):
		new_piece_at(types[i], Facing.DOWN, Vector2(i, 0))
		new_piece_at(types[i], Facing.DOWN, Vector2(8 - i, 0))
		new_piece_at(types[i], Facing.UP, Vector2(i, 8))
		new_piece_at(types[i], Facing.UP, Vector2(8 - i, 8))
	
	new_piece_at("king", Facing.DOWN, Vector2(4, 0))
	new_piece_at("king", Facing.UP, Vector2(4, 8))

func king_pos():
	"""
	Returns the position of the current players king.
	"""
	for k in board.pieces.keys():
		if board.pieces[k].type == "king" and board.pieces[k].dir == current_player:
			return k

func piece_moves(cell_v, as_opponent = false):
	"""
	Calculates all possible board positions for a piece.
	@param cell_v: position of the piece to calculate for
	"""
	return board.pieces[cell_v].next_positions(board, cell_v, as_opponent)

func interpolate(from, to):
	var diff = to - from
	var delta = abs(diff.x if int(diff.x) != 0 else diff.y)
	diff /= delta
	var res = []
	for i in range(delta + 1):
		res.append(from + i * diff)
	return res

func get_board_state(): #TODO Make nicer
	"""
	Calculates a state object containing all information needed for a turn.
	"""
	var player_moves = {} 
	var enemy_moves = {}
	var enemy_moves_simple = {}
	var critical_moves = []
	var k = king_pos()
	var blocked = []
	var state = {"alert_pos": [], "critical_moves": []}
	var critical_attacker
	
	for v in board.pieces.keys(): # Calculate initial movement
		if board.pieces[v].dir == current_player:
			player_moves[v] = piece_moves(v)
		else:
			enemy_moves[v] = piece_moves(v, true)
			enemy_moves_simple[v] = piece_moves(v)
			if enemy_moves[v].has(k):
				critical_moves = interpolate(k, v)
				state["critical_moves"] = critical_moves
				critical_attacker = v
				for i in range(critical_moves.size() - 1):
					var c = critical_moves[i]
					if board.pieces.has(c) and board.pieces[c].type != "king": # TODO Deal with King better
						blocked.append(c)
	
	if not critical_moves.empty(): # force check prevention
		if blocked.size() == 1:
			player_moves[blocked[0]] = intersect(player_moves[blocked[0]], critical_moves)
		elif blocked.empty():
			for p in player_moves.keys():
				if board.pieces[p].type != "king":
					player_moves[p] = intersect(player_moves[p], critical_moves)
			state["alert_pos"].append(critical_moves.pop_back())
			cut(player_moves[k], enemy_moves[critical_attacker], true)

	for e in enemy_moves_simple.values(): # don't allow king to move into check
		cut(player_moves[k], e, true)
	
	clean(player_moves)
	
	state["player_moves"] = player_moves
	state["blocked"] = blocked != null
	state["alert_pos"].append(critical_moves.pop_front())
	return state

func intersect(arr_a, arr_b):
	"""
	Returns the intersection of two arrays.
	"""
	var res = []
	for i in arr_a:
		if arr_b.has(i):
			res.append(i)
	return res

func cut(arr_a, arr_b, mutate=false):
	"""
	Removes element from arr_b in arr_a.
	"""
	var res = arr_a if mutate else arr_a.duplicate()
	for i in arr_b:
		res.erase(i)
	return res
	
func can_promote(selected, v):
	var promo = false
	if current_state == TurnState.PIECE_SELECTED:
		var piece = board.pieces[selected]
		promo = not ["king", "gold"].has(piece.type) and not piece["promoted"]
	elif current_state == TurnState.RELEASE_SELECTED:
		promo = not ["king", "gold"].has(selected)
	return promo and (
		current_player == Facing.UP and v.y <= 2 or
		current_player == Facing.DOWN and v.y >= 6
	)

func process_tile_info(state, v):
	"""
	Calculates relevant information for a given tile.
	@param state: board state to use as a calculation basis
	@param v: position vector of the relevant tile
	"""
	var info = {"can_move": false, "critical": false, "can_promote": false}
	if board.pieces.has(v):
		info["piece"] = board.pieces[v]

	# general coloring
	if state["alert_pos"].has(v) and not state["blocked"]:
		info["critical"] = true
	
	match current_state:
		TurnState.START:
			if state["player_moves"].has(v):
				info["can_move"] = true
		TurnState.PIECE_SELECTED:
			info["can_move"] = state["player_moves"][selected].find(v) >= 0
			info["can_promote"] = info["can_move"] and can_promote(selected, v)
		TurnState.RELEASE_SELECTED:
			info["can_move"] = state["release_options"][selected].has(v)
	
	return info

func render_board():
	"""
	Conveys state information to all tiles to render them.
	"""
	for k in tiles.keys():
		tiles[k].draw_tile(process_tile_info(board_state, k))

func _ready():
	board = preload("res://Scripts/Data/Board.gd").new()
	for tile in $Tiles.get_children():
		tiles[Vector2(tile.col, tile.row)] = tile
	
	trays = {
		Facing.DOWN: $TrayDown,
		Facing.UP: $TrayUp
	}
	trays[Facing.UP].flip_labels()
	
	generate_standard_game()
	board_state = get_board_state()
	render_board()

func find_free(y_off):
	var start_y = y_off if current_player == Facing.UP else 0
	var end_y = 9 if current_player == Facing.UP else 9 - y_off
	var res = {}
	
	for x in range(9):
		for y in range(start_y, end_y):
			var v = Vector2(x, y)
			if not board.pieces.has(v):
				res[v] = true
	return res

func cols_without_pawns():
	var cs = range(9)
	for k in board.pieces.keys():
		var p = board.pieces[k]
		if p.type == "pawn" and allies(p.dir): # for all the players pawns
			cs.erase(int(k.x))
	return cs

func clean(dict):
	for d in dict.keys():
		if dict[d].empty():
			dict.erase(d)

func calc_release_options(state):
	var rel_opts = {}
	
	var cm = state["critical_moves"]
	
	var res = []
	for t in TYPES:
		match t:
			"pawn":
				var cs = cols_without_pawns()
				res = find_free(1)
				for k in res.keys():
					if not cs.has(int(k.x)):
						res.erase(k)
			"knight":
				res = find_free(2)
			"lance":
				res = find_free(1)
			_:
				res = find_free(0)
		
		if not cm.empty():
			res = intersect(res, cm)
		rel_opts[t] = res
	
	clean(rel_opts)
	return rel_opts

func _on_release(type):
	current_state = TurnState.RELEASE_SELECTED
	selected = type
	render_board()

func _on_BoardTile_pressed(cell_v):
	"""
	Executed if any board tile is pressed.
	@param cell_v: Position vector of the clicked tile
	"""
	var last_moved
	match current_state:
		TurnState.START:
			selected = cell_v
			current_state = TurnState.PIECE_SELECTED
		TurnState.PIECE_SELECTED:
			last_moved = board.pieces[selected]
			var cap = board.make_move(selected, cell_v)
			if cap != null:
				trays[current_player].captured(cap)
		TurnState.RELEASE_SELECTED:
			last_moved = new_piece_at(selected, current_player, cell_v)
	
	if last_moved != null:
		if last_moved.must_promote(cell_v):
			last_moved.promote()
			switch_player()
		elif last_moved.may_promote(board, cell_v):
			selected = last_moved
			current_state = TurnState.MAY_PROMOTE
			$Panel.show()
		else:
			switch_player()
	render_board()

func _on_Promotion_Press(confirm):
	if confirm:
		selected.promote()
	$Panel.hide()
	switch_player()
	render_board()
