extends Node2D

signal release(tray, type)

var types = {}

func _ready():
	for c in $Panels.get_children():
		types[c.type] = c
		c.connect("clicked", self, "release")

func flip_labels():
	for v in types.values():
		v.flip_labels()

func disable():
	for v in types.values():
		v.disable()

func disable_one(type):
	types[type].disable()

func enable():
	for v in types.values():
		v.enable()

func captured(piece):
	types[piece["type"]].add_piece()

func release(type):
	types[type].remove_piece()
	disable()
	emit_signal("release", type)

