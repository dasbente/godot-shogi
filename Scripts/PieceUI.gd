extends Control

##### RENDER LOGIC #####
func set_piece(type):
	$Label.animation = type
	
	if type == "king" or type == "pawn":
		$Piece.animation = type
	elif type == "rook" or type == "bishop":
		$Piece.animation = "major"
	else:
		$Piece.animation = "minor"