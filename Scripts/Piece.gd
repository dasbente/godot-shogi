extends Node2D

var Facing = enums.Facing

##### RENDER LOGIC #####
func set_piece(piece):
	var type = piece["type"]
	$Label.animation = type
	promote(piece["promoted"])
	update_orientation(piece["dir"])
	
	if type == "king" or type == "pawn":
		$Piece.animation = type
	elif type == "rook" or type == "bishop":
		$Piece.animation = "major"
	else:
		$Piece.animation = "minor"

func update_orientation(dir):
	$Piece.set_flip_v(dir == Facing.DOWN)

func promote(promo):
	if promo:
		$Label.frame = 1