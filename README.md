# godot-shogi
A simple implementation of the board game [Shogi](https://en.wikipedia.org/wiki/Shogi) 
in the Godot Engine.

## TODO
- piece promotions
- finish gameplay loop (end game when the king is beaten)
- UI (main menu, move history, clock)
- different modes (various handicaps, maybe shogi problems, some more exotic variants?)
- CPU opponent?